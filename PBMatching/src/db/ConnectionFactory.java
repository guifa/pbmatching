package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	private static final String DB_DRIVER = "org.postgresql.Driver";
	private static final String DB_CONNECTION = "jdbc:postgresql://127.0.0.1:5432/postgres?autoReconnect=true";
	private static final String DB_USER = "postgres";
	private static final String DB_PASSWORD = "098765";
    
	public Connection getConnection() {
        try {
        	try {
        		Class.forName(DB_DRIVER);        		
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}        	
            return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
	
}