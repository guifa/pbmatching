package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import geometry.CandidatePoint;
import geometry.Point;
import geometry.TrajectoryPoint;

public class CandidateDAO {
	
	private Connection connection;
	private PreparedStatement pstmt;
	private Statement stmt;
	private ResultSet rs;
	private static final String TABLE_NAME = "candidate_points";

	public CandidateDAO() {
		
	}
	
	public void deleteCandidatePoints(){
		String sql = "DELETE FROM " + TABLE_NAME;
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
	
	public void AddCandidatePoint(CandidatePoint candidatePoint){
		String sql = "INSERT INTO " + TABLE_NAME
				+ "("
				+ "id,"				
				+ "trajectory_point_id,"
				+ "edge_id,"
				+ "trajectory_point_to_candidate_point_distance,"
				+ "geom"
				+ ") VALUES"
				+ "(?,?,?,?,ST_SetSRID(ST_MakePoint(?,?),4326))";
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	pstmt = connection.prepareStatement(sql);
	    	pstmt.setInt(1, candidatePoint.getID());
	    	pstmt.setInt(2, candidatePoint.getTrajectoryPoint().getID());
	    	pstmt.setLong(3, candidatePoint.getEdgeID());
	    	pstmt.setDouble(4, candidatePoint.getDistance());
	    	pstmt.setDouble(5, candidatePoint.getX());
	    	pstmt.setDouble(6, candidatePoint.getY());
	    	pstmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
	
	public ResultSet getCandidatees(TrajectoryPoint trajectoryPoint){
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE Trajectory_Point_ID = " + trajectoryPoint.getID();
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql);
	    	return rs;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}

}
