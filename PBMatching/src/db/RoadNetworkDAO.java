package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import geometry.Point;
import geometry.TrajectoryPoint;
import geometry.CandidatePoint;

public class RoadNetworkDAO {
	
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private static final String EDGES_TABLE_NAME = "fortaleza_sample_edges_xy";
	private static final String VERTICES_TABLE_NAME = "fortaleza_sample_nodes_xy";
	
	public void createRoadNetworkFromFile(String nodeFile,String edgeFile) throws IOException, SQLException {
        
        BufferedReader br = new BufferedReader(new FileReader(new File(nodeFile)));
        
        String sql;
        String line = "";
        while((line = br.readLine())!=null){
            String[] l = line.split(";");

            Integer ID  = Integer.parseInt(l[0]);
            Double x = Double.parseDouble(l[2]);
            Double y = Double.parseDouble(l[1]);
            
            sql = "INSERT INTO " + VERTICES_TABLE_NAME
    				+ "("
    				+ "ID,"
    				+ "X,"
    				+ "Y,"
    				+ "geom"
    				+ ") VALUES"
    				+ "(?,?,?,?)";
            
            try {
    	    	connection = new ConnectionFactory().getConnection();
    	    	pstmt = connection.prepareStatement(sql);
    	    	pstmt.setInt(1, ID);
    	    	pstmt.setDouble(2, x);
    	    	pstmt.setDouble(3, y);
    	    	pstmt.setString(4, "ST_SetSRID(ST_MakePoint(" + x + "," + y + "),4326))");
    	    	pstmt.execute();
    		} catch (SQLException e) {
    			throw new RuntimeException(e.getMessage(), e);
    		} finally {
    			try {
    				connection.close();
    				pstmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}			
    		}
        }
        br.close();

        br = new BufferedReader(new FileReader(new File(edgeFile)));

        line = "";
        while((line = br.readLine())!=null){
            String[] l = line.split(";");
            
            Integer ID     =  Integer.parseInt(l[0]);
            Integer source =  Integer.parseInt(l[1]);
            Integer target =  Integer.parseInt(l[2]);
            
            String sSource;
            sSource = getCrossroad(l[1]);
            
            String sTarget;
            sTarget = getCrossroad(l[2]);
            
            sql = "INSERT INTO " + EDGES_TABLE_NAME
    				+ "("
    				+ "ID,"
    				+ "Source,"
    				+ "Target,"
    				+ "geom"
    				+ ") VALUES"
    				+ "(?,?,?,?)";
            
            try {
    	    	connection = new ConnectionFactory().getConnection();
    	    	pstmt = connection.prepareStatement(sql);
    	    	pstmt.setInt(1, ID);
    	    	pstmt.setInt(2, source);
    	    	pstmt.setInt(3, target);
    	    	pstmt.setString(4, "ST_MakeLine(" + sSource + "," + sTarget + ")");
    	    	pstmt.execute();
    		} catch (SQLException e) {
    			throw new RuntimeException(e.getMessage(), e);
    		} finally {
    			try {
    				connection.close();
    				pstmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}			
    		}
        }
        br.close();
    }	
	
	public String getCrossroad(String ID) throws SQLException{
		String sql = "SELECT geom FROM " + VERTICES_TABLE_NAME + " WHERE id = ?";
		String result = "";
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	pstmt = connection.prepareStatement(sql);
	    	pstmt.setString(1, ID);
	    	pstmt.execute();
	    	while(rs.next()){
				result = rs.getString(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				pstmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
		return result;		
	}
	
	public List<CandidatePoint> FindCandidates(TrajectoryPoint trajectoryPoint, double radius, int maxCandidateCount) throws Exception{
	    String trajectoryPointString = trajectoryPoint.toString();
	    System.out.println("Find Candidate: " + trajectoryPointString);
	    String sql = "SELECT"
	    		+ " id as Edge_ID,"
		    	+ " ST_AsText(the_geom) as Edge,"
		    	+ " ST_Distance(the_geom::geography, ST_GeogFromText("+ trajectoryPointString +")) as Candidate_Point_To_Edge_Distance,"
		    	+ " ST_AsText(ST_ClosestPoint(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326))) as Candidate_Point,"
	    		+ " ST_X(ST_ClosestPoint(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326))) as Candidate_Point_X,"
	    		+ " ST_Y(ST_ClosestPoint(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326))) as Candidate_Point_Y,"
	    		+ " popularity as Road_Popularity"
	    		+ " FROM " + EDGES_TABLE_NAME
	    		+ " WHERE ST_Dwithin(the_geom::geography, ST_GeogFromText("+ trajectoryPointString +"), "+ radius +")"
	    		+ " ORDER BY Candidate_Point_To_Edge_Distance "
	    		+ " limit "+ maxCandidateCount +"";
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	System.out.println(sql);
	    	rs = stmt.executeQuery(sql);
	    	int id = 1;
	    	List<CandidatePoint> candidatePoints = new ArrayList<>();
	    	CandidateDAO candidateDAO = new CandidateDAO();
	    	while(rs.next()){
	    		CandidatePoint candidatePoint = new CandidatePoint(id, rs.getDouble("Candidate_Point_X"), rs.getDouble("Candidate_Point_Y"), trajectoryPoint, rs.getLong("Edge_ID"), rs.getDouble("Candidate_Point_To_Edge_Distance"), rs.getDouble("Road_Popularity"));
	    		candidateDAO.AddCandidatePoint(candidatePoint);
	    		candidatePoints.add(candidatePoint);
	    		id++;
	    	}
	    	if(candidatePoints.isEmpty()){
	        	throw new Exception("Can't find any candidate for:" + trajectoryPoint + " - SQL: " + sql);
//	            System.out.println("Can't find any candidate for:" + originalPoint);
//	            result = FindCandidatePoints(originalPoint, edges, 100, maxCandidateCount);
	        }
	    	return candidatePoints;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				if (rs != null){
					rs.close();
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	    
	}
	
	public List<CandidatePoint> FindCandidatesXY(TrajectoryPoint trajectoryPoint, double radius, int maxCandidateCount) throws Exception{
	    String trajectoryPointString = trajectoryPoint.toString();	    
	    String sql = "SELECT"
	    		+ " id as Edge_ID,"
		    	+ " ST_AsText(the_geom) as Edge,"
		    	+ " ST_Distance(the_geom, ST_GeomFromText("+ trajectoryPointString +", 4326)) as Candidate_Point_To_Edge_Distance,"
		    	+ " ST_AsText(ST_ClosestPoint(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326))) as Candidate_Point,"
	    		+ " ST_X(ST_ClosestPoint(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326))) as Candidate_Point_X,"
	    		+ " ST_Y(ST_ClosestPoint(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326))) as Candidate_Point_Y,"
	    		+ " popularity as Road_Popularity"
	    		+ " FROM " + EDGES_TABLE_NAME
	    		+ " WHERE ST_Dwithin(the_geom, ST_GeomFromText("+ trajectoryPointString +", 4326), "+ radius +")"
	    		+ " ORDER BY Candidate_Point_To_Edge_Distance "
	    		+ " limit "+ maxCandidateCount +"";
//	    System.out.println(sql);
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql);
	    	int id = 1;
	    	List<CandidatePoint> candidatePoints = new ArrayList<>();
	    	CandidateDAO candidateDAO = new CandidateDAO();
	    	while(rs.next()){
//	    		System.out.println("Candidate Point: " + rs.getString("Candidate_Point"));
//	    		System.out.println("X: " + rs.getDouble("Candidate_Point_X"));
//	    		System.out.println("Y: " + rs.getDouble("Candidate_Point_Y"));
	    		
	    		CandidatePoint candidatePoint = new CandidatePoint(id, rs.getDouble("Candidate_Point_X"), rs.getDouble("Candidate_Point_Y"), trajectoryPoint, rs.getLong("Edge_ID"), rs.getDouble("Candidate_Point_To_Edge_Distance"), rs.getDouble("Road_Popularity"));
	    		candidateDAO.AddCandidatePoint(candidatePoint);
	    		candidatePoints.add(candidatePoint);
	    		id++;
	    	}
	    	if(candidatePoints.isEmpty()){
	        	throw new Exception("Can't find any candidate for:" + trajectoryPoint + " - SQL: " + sql);
//	            System.out.println("Can't find any candidate for:" + originalPoint);
//	            result = FindCandidatePoints(originalPoint, edges, 100, maxCandidateCount);
	        }
	    	return candidatePoints;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	    
	}

}
