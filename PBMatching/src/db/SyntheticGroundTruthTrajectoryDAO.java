package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import geometry.CandidatePoint;
import geometry.Point;
import geometry.Trajectory;
import geometry.TrajectoryPoint;

public class SyntheticGroundTruthTrajectoryDAO {
			
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private static final String TRAJECTORY_TABLE_NAME = "synthetic_ground_truth_trajectory";
	private static final String EDGES_TABLE_NAME = "fortaleza_sample_edges_xy";
	
	public static double latitudeY(double latitude) {
		return 6378137 * Math.log(Math.tan(Math.PI / 4 + Math.toRadians(latitude) / 2));
	}

	public static double longitudeX(double longitude){
		return 6378137 * Math.toRadians(longitude);
	}
	
	public static double metersYtoLatitude(double Y){
		return Math.toDegrees(Math.atan(Math.sinh(Y / 6378137)));
	}
	
	public static double meterXtoLongitude(double X){
		return Math.toDegrees(X/6378137);
	}
	
	public void deleteSyntethicGroundTruthTrajectory(){
		String sql = "DELETE FROM " + TRAJECTORY_TABLE_NAME;
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
	
	public Trajectory getSyntethicGroundTruthTrajectory(){   
	    String sql = "SELECT"
	    		+ " id,"
		    	+ " x,"
		    	+ " y,"
		    	+ " time_stamp,"
		    	+ " edge_id"
	    		+ " FROM " + TRAJECTORY_TABLE_NAME;
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql);
	    	List<TrajectoryPoint> trajectoryPoints = new ArrayList<>();
	    	while(rs.next()){
	    		TrajectoryPoint trajectoryPoint = new TrajectoryPoint(rs.getInt("id"), rs.getDouble("x"), rs.getDouble("y"), 
	    												rs.getDouble("time_stamp"), rs.getLong("edge_id"));
	    		trajectoryPoints.add(trajectoryPoint);
	    	}
	    	Trajectory trajectory = new Trajectory(trajectoryPoints);
	    	return trajectory;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	    
	}
	
	//Works with Longitude and Latitude format.
	public List<TrajectoryPoint> closestLineProjection(List<TrajectoryPoint> trajectory) throws SQLException{
		List<TrajectoryPoint> correctedTrajectory = new ArrayList<>();
		for (TrajectoryPoint trajectoryPoint : trajectory){
			String trajectoryPointString = trajectoryPoint.toString();
			String sql = "SELECT"
					+ " id as Edge_ID,"
					+ " ST_Distance(the_geom::geography, ST_GeogFromText("+ trajectoryPointString +")) as Candidate_Point_To_Edge_Distance,"
		    		+ " ST_X(ST_Line_Interpolate_Point(the_geom, ST_Line_Locate_Point(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326)))) as Point_X,"
		    		+ " ST_Y(ST_Line_Interpolate_Point(the_geom, ST_Line_Locate_Point(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326)))) as Point_Y"
		    		+ " FROM " + EDGES_TABLE_NAME 
		    		+ " WHERE ST_Dwithin(the_geom::geography, ST_GeogFromText("+ trajectoryPointString +"), "+ 10 +")"
		    		+ " ORDER BY Candidate_Point_To_Edge_Distance "
		    		+ " limit "+ 1 +"";
			try{
				connection = new ConnectionFactory().getConnection();
		    	stmt = connection.createStatement();
				rs = stmt.executeQuery(sql);
//				System.out.println(sql);
				while(rs.next()){
					TrajectoryPoint correctedTrajectoryPoint = new TrajectoryPoint(trajectoryPoint.getID(), 
							rs.getDouble("Point_X"), rs.getDouble("Point_Y"), trajectoryPoint.getTime(), rs.getLong("Edge_ID"));
//					System.out.println("Trajectory Point: " + correctedTrajectoryPoint.toString());
					correctedTrajectory.add(correctedTrajectoryPoint);
				}
			}catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					connection.close();
					stmt.close();
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}			
			}				
		}
		return correctedTrajectory;			
	}
	//Works with X and Y format
	public List<TrajectoryPoint> closestLineProjectionXY(List<TrajectoryPoint> trajectory) throws SQLException{
		List<TrajectoryPoint> correctedTrajectory = new ArrayList<>();
		for (TrajectoryPoint trajectoryPoint : trajectory){
			String trajectoryPointString = trajectoryPoint.toString();
			String sql = "SELECT"
					+ " id as Edge_ID,"
					+ " ST_Distance(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326)) as Candidate_Point_To_Edge_Distance,"
		    		+ " ST_X(ST_Line_Interpolate_Point(the_geom, ST_Line_Locate_Point(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326)))) as Point_X,"
		    		+ " ST_Y(ST_Line_Interpolate_Point(the_geom, ST_Line_Locate_Point(the_geom, ST_GeomFromText("+ trajectoryPointString +",4326)))) as Point_Y"
		    		+ " FROM " + EDGES_TABLE_NAME 
		    		+ " ORDER BY Candidate_Point_To_Edge_Distance "
		    		+ " limit "+ 1 +"";
			try{
				connection = new ConnectionFactory().getConnection();
		    	stmt = connection.createStatement();
				rs = stmt.executeQuery(sql);
//				System.out.println(sql);
				while(rs.next()){
					TrajectoryPoint correctedTrajectoryPoint = new TrajectoryPoint(trajectoryPoint.getID(), 
							rs.getDouble("Point_X"), rs.getDouble("Point_Y"), trajectoryPoint.getTime(), rs.getLong("Edge_ID"));
					correctedTrajectory.add(correctedTrajectoryPoint);
				}
			}catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					connection.close();
					stmt.close();
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}			
			}				
		}
		return correctedTrajectory;			
	}
	
	//Provavelmente s� vai funcionar nesse exemplo
	public void insertSumoSyntheticGroundTruthTrajecoty(String trajectoryFile) 
			throws NumberFormatException, IOException, SQLException{
		//Delete any data on the table before insert.
		deleteSyntethicGroundTruthTrajectory();
		BufferedReader br = new BufferedReader(new FileReader(new File(trajectoryFile)));
		List<TrajectoryPoint> syntheticTrajecotry = new ArrayList<>();
		List<TrajectoryPoint> correctedTrajectory = new ArrayList<>();
        String line = "";
        int trajecotryPointID = 1;
        while((line = br.readLine())!=null){
            String[] l = line.split(",");            
//            Double x = Double.parseDouble(l[0]);//Used with longitude values.
//            Double y = Double.parseDouble(l[1]);//Used with latitude values.
            
            Double x = longitudeX(Double.parseDouble(l[0]));//Used with X values.
            Double y = latitudeY(Double.parseDouble(l[1]));//Used with Y values.
            
            Double timeStamp = Double.parseDouble(l[2]);
//            System.out.println("X: " + x + ", Y: " + y + ", time: " + timeStamp);
            TrajectoryPoint trajectoryPoint = 
            		new TrajectoryPoint(trajecotryPointID, x, y, timeStamp, -1);
            syntheticTrajecotry.add(trajectoryPoint);
            trajecotryPointID++;
        }
        br.close();
        
        correctedTrajectory = closestLineProjectionXY(syntheticTrajecotry);
        
        String sql = "INSERT INTO " + TRAJECTORY_TABLE_NAME	        		
				+ "("
				+ "id,"
				+ "edge_id,"
				+ "time_stamp,"
				+ "x,"
				+ "y,"
				+ "geom"
				+ ") VALUES"
				+ "(?,?,?,?,?,ST_SetSRID(ST_MakePoint(?,?),4326))";
        
        for(TrajectoryPoint correctedTrajecotryPoint : correctedTrajectory){
        	try {
    	    	connection = new ConnectionFactory().getConnection();
    	    	pstmt = connection.prepareStatement(sql);
    	    	pstmt.setInt(1, correctedTrajecotryPoint.getID());
    	    	pstmt.setLong(2, correctedTrajecotryPoint.getEdgeID());
    	    	pstmt.setDouble(3, correctedTrajecotryPoint.getTime());
    	    	pstmt.setDouble(4, correctedTrajecotryPoint.getX());
    	    	pstmt.setDouble(5, correctedTrajecotryPoint.getY());
    	    	pstmt.setDouble(6, correctedTrajecotryPoint.getX());
    	    	pstmt.setDouble(7, correctedTrajecotryPoint.getY());
    	    	pstmt.execute();
    		} catch (SQLException e) {
    			throw new RuntimeException(e.getMessage(), e);
    		} finally {
    			try {
    				connection.close();
    				pstmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}			
    		}
        }	        
	}

}

