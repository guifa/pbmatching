package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import geometry.CandidatePoint;
import geometry.Trajectory;
import geometry.TrajectoryPoint;

public class SyntheticMatchedTrajectoryDAO {
	
	private Connection connection = null;
	private PreparedStatement pstmt = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private static final String TRAJECTORY_TABLE_NAME = "synthetic_matched_trajectory";
	
	public void deleteSyntethicMatchedTrajectory(){
		String sql = "DELETE FROM " + TRAJECTORY_TABLE_NAME;
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
	
	public void deleteError(){
		String sql = "DELETE FROM " + TRAJECTORY_TABLE_NAME + "_error";
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
	
	public Trajectory getSyntethicMatchedTrajectory(){   
	    String sql = "SELECT"
	    		+ " id,"
	    		+ " edge_id,"
		    	+ " x,"
		    	+ " y,"
		    	+ " time_stamp"
	    		+ " FROM " + TRAJECTORY_TABLE_NAME;
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql);
	    	List<TrajectoryPoint> trajectoryPoints = new ArrayList<>();
	    	while(rs.next()){
	    		TrajectoryPoint trajectoryPoint = new TrajectoryPoint(rs.getInt("id"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("time_stamp"), rs.getLong("edge_id"));
	    		trajectoryPoints.add(trajectoryPoint);
	    	}
	    	Trajectory trajectory = new Trajectory(trajectoryPoints);
	    	return trajectory;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}	    
	}
	
	//Get all the trajectory points that were matched incorrectly during the map-matching.
	public Trajectory getError(){
		String sql = "SELECT"
	    		+ " smt.id,"
	    		+ " smt.edge_id,"
		    	+ " smt.x,"
		    	+ " smt.y,"
		    	+ " smt.time_stamp"
	    		+ " FROM "
	    		+ " synthetic_ground_truth_trajectory sgtt, "+ TRAJECTORY_TABLE_NAME +" smt"
	    		+ " WHERE"
	    		+ " sgtt.id = smt.id and ABS(sgtt.edge_id) != ABS(smt.edge_id)";
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql);
	    	List<TrajectoryPoint> trajectoryPoints = new ArrayList<>();
	    	while(rs.next()){
	    		TrajectoryPoint trajectoryPoint = new TrajectoryPoint(rs.getInt("id"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("time_stamp"), rs.getLong("edge_id"));
	    		trajectoryPoints.add(trajectoryPoint);
	    	}
	    	Trajectory trajectory = new Trajectory(trajectoryPoints);
	    	return trajectory;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
		
	public void insertSumoSyntheticMatchedTrajecory(Trajectory syntheticMatchedTrajectory) throws NumberFormatException, IOException, SQLException{
		//Delete any data on the table before insert.
		deleteSyntethicMatchedTrajectory();
        String sql = "INSERT INTO " + TRAJECTORY_TABLE_NAME	        		
				+ "("
				+ "id,"
				+ "edge_id,"
				+ "time_stamp,"
				+ "x,"
				+ "y,"
				+ "geom"
				+ ") VALUES"
				+ "(?,?,?,?,?,ST_SetSRID(ST_MakePoint(?,?),4326))";
        
        for(TrajectoryPoint trajecotryPoint : syntheticMatchedTrajectory.getTrajectoryPoints()){
        	try {
    	    	connection = new ConnectionFactory().getConnection();
    	    	pstmt = connection.prepareStatement(sql);
    	    	pstmt.setInt(1, trajecotryPoint.getID());
    	    	pstmt.setLong(2, trajecotryPoint.getEdgeID());
    	    	pstmt.setDouble(3, trajecotryPoint.getTime());
    	    	pstmt.setDouble(4, trajecotryPoint.getX());
    	    	pstmt.setDouble(5, trajecotryPoint.getY());
    	    	pstmt.setDouble(6, trajecotryPoint.getX());
    	    	pstmt.setDouble(7, trajecotryPoint.getY());
    	    	pstmt.execute();
    		} catch (SQLException e) {
    			throw new RuntimeException(e.getMessage(), e);
    		} finally {
    			try {
    				connection.close();
    				pstmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}			
    		}
        }
	}
	
	public double checkAccuracy(){
		double accuracy;
		double totalTrajectories;
		double correctTrajectories;
		String sql1 = "SELECT"
	    		+ " count(*)"
	    		+ " FROM"
	    		+ " synthetic_ground_truth_trajectory";
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql1);
	    	rs.next();
	    	totalTrajectories = rs.getInt(1);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		String sql2 = "SELECT"
	    		+ " count(*)"
	    		+ " FROM"
	    		+ " synthetic_ground_truth_trajectory sgtt, "+ TRAJECTORY_TABLE_NAME +" smt"
	    		+ " WHERE"
	    		+ " sgtt.id = smt.id and ABS(sgtt.edge_id) = ABS(smt.edge_id)";
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql2);
	    	rs.next();
	    	correctTrajectories = rs.getInt(1);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
//    	System.out.println("correctTrajectories: " + correctTrajectories);
//    	System.out.println("totalTrajectories: " + totalTrajectories);
	    accuracy = (correctTrajectories/totalTrajectories)*100;
//	    System.out.println("accuracy: " + accuracy);
		return accuracy;
	}
	
	//Insert trajectory points that were matched incorrectly during the map-matching into the database.
	public void insertError(){
		//Delete everything before insert.
		deleteError();
		Trajectory errorTrajectory = new Trajectory(getError().getTrajectoryPoints());
		String sql = "INSERT INTO " + TRAJECTORY_TABLE_NAME + "_error"
				+ "("
				+ "id,"
				+ "edge_id,"
				+ "time_stamp,"
				+ "x,"
				+ "y,"
				+ "geom"
				+ ") VALUES"
				+ "(?,?,?,?,?,ST_SetSRID(ST_MakePoint(?,?),4326))";
        
        for(TrajectoryPoint trajecotryPoint : errorTrajectory.getTrajectoryPoints()){
        	try {
    	    	connection = new ConnectionFactory().getConnection();
    	    	pstmt = connection.prepareStatement(sql);
    	    	pstmt.setInt(1, trajecotryPoint.getID());
    	    	pstmt.setLong(2, trajecotryPoint.getEdgeID());
    	    	pstmt.setDouble(3, trajecotryPoint.getTime());
    	    	pstmt.setDouble(4, trajecotryPoint.getX());
    	    	pstmt.setDouble(5, trajecotryPoint.getY());
    	    	pstmt.setDouble(6, trajecotryPoint.getX());
    	    	pstmt.setDouble(7, trajecotryPoint.getY());
    	    	pstmt.execute();
    		} catch (SQLException e) {
    			throw new RuntimeException(e.getMessage(), e);
    		} finally {
    			try {
    				connection.close();
    				pstmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}			
    		}
        }
	}

}
