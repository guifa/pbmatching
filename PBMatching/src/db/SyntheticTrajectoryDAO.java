package db;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;

import geometry.CandidatePoint;
import geometry.Trajectory;
import geometry.TrajectoryPoint;

import db.SyntheticGroundTruthTrajectoryDAO;

public class SyntheticTrajectoryDAO {
	
	private Connection connection;
	private PreparedStatement pstmt;
	private Statement stmt;
	private ResultSet rs;
	private static final String TRAJECTORY_TABLE_NAME = "synthetic_trajectory";
	
	public static double latitudeY(double latitude) {
		return 6378137 * Math.log(Math.tan(Math.PI / 4 + Math.toRadians(latitude) / 2));
	}

	public static double longitudeX(double longitude){
		return 6378137 * Math.toRadians(longitude);
	}
	
	public static double metersYtoLatitude(double Y){
		return Math.toDegrees(Math.atan(Math.sinh(Y / 6378137)));
	}
	
	public static double meterXtoLongitude(double X){
		return Math.toDegrees(X/6378137);
	}
	
	public void deleteSyntethicTrajectory(){
		String sql = "DELETE FROM " + TRAJECTORY_TABLE_NAME;
		try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	stmt.executeUpdate(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	}
	
	public Trajectory getSyntheticTrajectory(){   
	    String sql = "SELECT"
	    		+ " id,"
		    	+ " x,"
		    	+ " y,"
		    	+ " time_stamp"
		    	+ " FROM " + TRAJECTORY_TABLE_NAME
	    		+ " ORDER BY id";
	    try {
	    	connection = new ConnectionFactory().getConnection();
	    	stmt = connection.createStatement();
	    	rs = stmt.executeQuery(sql);
	    	List<TrajectoryPoint> trajectoryPoints = new ArrayList<>();
	    	while(rs.next()){
	    		TrajectoryPoint trajectoryPoint = new TrajectoryPoint(rs.getInt("id"), rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("time_stamp"), -1);
	    		trajectoryPoints.add(trajectoryPoint);
	    	}
	    	Trajectory trajectory = new Trajectory(trajectoryPoints);
	    	return trajectory;
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage(), e);
		} finally {
			try {
				connection.close();
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
	    
	}
	
	public void insertSumoSyntheticTrajecory() throws NumberFormatException, IOException, SQLException{
		//Delete any data on the table before insert.
		deleteSyntethicTrajectory();
		SyntheticGroundTruthTrajectoryDAO sgtt = new SyntheticGroundTruthTrajectoryDAO();
		Trajectory syntheticTrajecotry = shakeXYTrajectoryTest(sgtt.getSyntethicGroundTruthTrajectory());
        String sql = "INSERT INTO " + TRAJECTORY_TABLE_NAME	        		
				+ "("
				+ "id,"
				+ "time_stamp,"
				+ "x,"
				+ "y,"
				+ "geom"
				+ ") VALUES"
				+ "(?,?,?,?,ST_SetSRID(ST_MakePoint(?,?),4326))";
        
        for(TrajectoryPoint trajecotryPoint : syntheticTrajecotry.getTrajectoryPoints()){
        	try {
    	    	connection = new ConnectionFactory().getConnection();
    	    	pstmt = connection.prepareStatement(sql);
    	    	pstmt.setInt(1, trajecotryPoint.getID());
    	    	pstmt.setDouble(2, trajecotryPoint.getTime());
    	    	pstmt.setDouble(3, trajecotryPoint.getX());
    	    	pstmt.setDouble(4, trajecotryPoint.getY());
    	    	pstmt.setDouble(5, trajecotryPoint.getX());
    	    	pstmt.setDouble(6, trajecotryPoint.getY());
    	    	pstmt.execute();
    		} catch (SQLException e) {
    			throw new RuntimeException(e.getMessage(), e);
    		} finally {
    			try {
    				connection.close();
    				pstmt.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}			
    		}
        }	        
	}
	
	public static Trajectory shakeLongLatTrajectory(Trajectory trajectory){
		List<TrajectoryPoint> newTrajectory = new ArrayList<TrajectoryPoint>();        

        NormalDistribution nd = new NormalDistribution((double)0, (double)5);
        Random r = new Random(4);        

        for (TrajectoryPoint trajectoryPoint : trajectory.getTrajectoryPoints()) {
        	TrajectoryPoint paux = null;
           
//           double latAux = latitudeY(p.getLat(), p.getLng());
//           double lngAux = longitudeX(p.getLat(), p.getLng());
//           double zAux = latlngZ(p.getLat());
//           System.out.println("original: :" + trajectoryPoint.toString());
           double latAux = latitudeY(trajectoryPoint.getY());
           double lngAux = longitudeX(trajectoryPoint.getX());
           
//           double latAux = 0;
//           double lngAux = 0;

           int n = Math.abs(r.nextInt())%4+1;

           //System.out.println(n);
           
           switch (n) {
           case 1: 
        	   latAux = latAux + (Math.abs(nd.sample()));
        	   lngAux = lngAux + (Math.abs(nd.sample()));
//        	   paux = new Point(metersYtoLatitude(lngAux, latAux, zAux), meterXtoLongitude(lngAux, latAux));
        	   
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), meterXtoLongitude(lngAux), metersYtoLatitude(latAux), trajectoryPoint.getTime(), -1);
        	   
//        	   latAux = p.getLat() + (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137));
//        	   lngAux = p.getLng() + (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137))/java.lang.Math.cos(Math.PI/180.0 * p.getLat());
//        	   paux = new Point(latAux, lngAux);        	   
        	   break;

           case 2:
        	   latAux = latAux - (Math.abs(nd.sample()));
        	   lngAux = lngAux + (Math.abs(nd.sample()));
//        	   paux = new Point(metersYtoLatitude(lngAux, latAux, zAux), meterXtoLongitude(lngAux, latAux));
        	   
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), meterXtoLongitude(lngAux), metersYtoLatitude(latAux), trajectoryPoint.getTime(), -1);
        	   
//        	   latAux = p.getLat() - (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137));
//        	   lngAux = p.getLng() + (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137))/java.lang.Math.cos(Math.PI/180.0 * p.getLat());
//        	   paux = new Point(latAux, lngAux);
        	   break;
           
           case 3:
        	   latAux = latAux + (Math.abs(nd.sample()));
        	   lngAux = lngAux - (Math.abs(nd.sample()));
//        	   paux = new Point(metersYtoLatitude(lngAux, latAux, zAux), meterXtoLongitude(lngAux, latAux));
        	   
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), meterXtoLongitude(lngAux), metersYtoLatitude(latAux), trajectoryPoint.getTime(), -1);
        	   
//        	   latAux = p.getLat() + (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137));
//        	   lngAux = p.getLng() - (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137))/java.lang.Math.cos(Math.PI/180.0 * p.getLat());
//        	   paux = new Point(latAux, lngAux);
        	   break;

           case 4:
        	   latAux = latAux - (Math.abs(nd.sample()));
        	   lngAux = lngAux - (Math.abs(nd.sample()));
//        	   paux = new Point(metersYtoLatitude(lngAux, latAux, zAux), meterXtoLongitude(lngAux, latAux));
        	   
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), meterXtoLongitude(lngAux), metersYtoLatitude(latAux), trajectoryPoint.getTime(), -1);
        	   
//        	   latAux = p.getLat() - (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137));
//        	   lngAux = p.getLng() - (180/java.lang.Math.PI)*((Math.abs(nd.sample())/6378137))/java.lang.Math.cos(Math.PI/180.0 * p.getLat());
//        	   paux = new Point(latAux, lngAux);
        	   break;

           }
//           System.out.println("With error: " + paux.toString());
           newTrajectory.add(paux);
        }
        return new Trajectory(newTrajectory);
	}
	
	public static Trajectory shakeXYTrajectory(Trajectory trajectory){
		List<TrajectoryPoint> newTrajectory = new ArrayList<TrajectoryPoint>();        

        NormalDistribution nd = new NormalDistribution((double)0, (double)5);
        Random r = new Random(4);        

        for (TrajectoryPoint trajectoryPoint : trajectory.getTrajectoryPoints()) {
        	TrajectoryPoint paux = null;    
           
           double latAux = trajectoryPoint.getY();
           double lngAux = trajectoryPoint.getX();
           
           int n = Math.abs(r.nextInt())%4+1;
           System.out.println(n);
           System.out.println(Math.abs(nd.sample()));
           
           switch (n) {
           case 1:
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux + (Math.abs(nd.sample()));
        	   lngAux = lngAux + (Math.abs(nd.sample()));
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);	   
        	   break;

           case 2:
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux - (Math.abs(nd.sample()));
        	   lngAux = lngAux + (Math.abs(nd.sample()));
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);
        	   break;
           
           case 3:
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux + (Math.abs(nd.sample()));
        	   lngAux = lngAux - (Math.abs(nd.sample()));
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);
        	   break;

           case 4:
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux - (Math.abs(nd.sample()));
        	   lngAux = lngAux - (Math.abs(nd.sample()));
        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);
        	   break;
           }      
           newTrajectory.add(paux);
        }
        return new Trajectory(newTrajectory);
	}
	
	public static Trajectory shakeXYTrajectoryTest(Trajectory trajectory){
		List<TrajectoryPoint> newTrajectory = new ArrayList<TrajectoryPoint>();        

        NormalDistribution nd = new NormalDistribution((double)0, (double)5);
        Random r = new Random(4);        

        for (TrajectoryPoint trajectoryPoint : trajectory.getTrajectoryPoints()) {
        	TrajectoryPoint paux = null;    
           
           double latAux = trajectoryPoint.getY();
           double lngAux = trajectoryPoint.getX();
           double sampleAux;
           
           int n = Math.abs(r.nextInt())%4+1;
//           System.out.println(n);
//           System.out.println(Math.abs(nd.sample()));
           
           switch (n) {
           case 1:
        	   sampleAux = Math.abs(nd.sample());
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux + (sampleAux);
        	   lngAux = lngAux + (sampleAux);
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);	   
        	   break;

           case 2:
        	   sampleAux = Math.abs(nd.sample());
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux - (sampleAux);
        	   lngAux = lngAux + (sampleAux);
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);
        	   break;
           
           case 3:
        	   sampleAux = Math.abs(nd.sample());
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux + (sampleAux);
        	   lngAux = lngAux - (sampleAux);
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);
        	   break;

           case 4:
        	   sampleAux = Math.abs(nd.sample());
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   latAux = latAux - (sampleAux);
        	   lngAux = lngAux - (sampleAux);
//        	   System.out.println("Long: " + lngAux + ", Lat: " + latAux);
        	   paux = new TrajectoryPoint(trajectoryPoint.getID(), lngAux, latAux, trajectoryPoint.getTime(), -1);
        	   break;
           }      
           newTrajectory.add(paux);
        }
        return new Trajectory(newTrajectory);
	}

}
