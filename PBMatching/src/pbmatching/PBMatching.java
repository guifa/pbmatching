package pbmatching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import db.CandidateDAO;
import db.RoadNetworkDAO;
import db.SyntheticTrajectoryDAO;
import geometry.CandidatePoint;
import geometry.Point;
import geometry.Trajectory;
import geometry.TrajectoryPoint;



public class PBMatching {
	
	private Trajectory trajectory;
    private double minObservationProbability = 0.0;
	private double maxObservationProbability = 0.0;
//    private static final double alfa = 1;
    private static final double radius = 50;
    private static final int maxCandidates = 5;
    private static final int numPoints = 35555;
    private static final double maxDifference = 3;
    
    public PBMatching(){
    	SyntheticTrajectoryDAO st = new SyntheticTrajectoryDAO();
    	this.trajectory = st.getSyntheticTrajectory();
    }
    
    public double Normalized(double e, double Emin, double Emax){
		return (e - Emin)/(Emax - Emin);    	
    }
    
    public List<List<CandidatePoint>> prepareData() throws Exception{
    	List<List<CandidatePoint>> aux = new ArrayList<List<CandidatePoint>>();
    	CandidateDAO candidateDAO = new CandidateDAO();
    	candidateDAO.deleteCandidatePoints();
    	RoadNetworkDAO roadNetworkDAO = new RoadNetworkDAO();
    	double observationProbability;
    	for(TrajectoryPoint trajectoryPoint : trajectory.getTrajectoryPoints()){
    		List<CandidatePoint> candidatePoints = new ArrayList<>();
//    		System.out.println("trajectoryPoint: " + trajectoryPoint.toString());
//    		System.out.println("TrajectoryPointID: " + trajectoryPoint.getID());
//    		System.out.println(trajectoryPoint.toString());
    		candidatePoints = roadNetworkDAO.FindCandidatesXY(trajectoryPoint, radius, maxCandidates);//Change to FindCandidatesXY, it may solve the problem.
//    		System.out.println("Size of candidatePoints: " + candidatePoints.size());
    		for(CandidatePoint candidatePoint : candidatePoints){
//    			System.out.println(candidatePoint.toString());
//    			System.out.println("distance: " + candidatePoint.getDistance());
    			observationProbability = candidatePoint.calculateObservationProbability();
//    			System.out.println("observationProbability: " + observationProbability);
    			if(observationProbability < minObservationProbability){
                	minObservationProbability = observationProbability;
//                	System.out.println("minObservationProbability = " + minObservationProbability);
                }
                if(observationProbability > maxObservationProbability){
                	maxObservationProbability = observationProbability;
//                	System.out.println("maxObservationProbability = " + maxObservationProbability);
                }
    		}
    		aux.add(candidatePoints);
    	}
    	return aux;
    }
    
    public Trajectory Match(double alfa) throws Exception{
    	Trajectory result = new Trajectory();
    	List<List<CandidatePoint>> aux = new ArrayList<List<CandidatePoint>>();
    	aux = prepareData();
    	Comparator<CandidatePoint> cmp = new Comparator<CandidatePoint>(){
			@Override
			public int compare(CandidatePoint c1, CandidatePoint c2) {
//				System.out.println("Comparing: " + c1.toString() + " to: " + c2.toString());
				double score1 = (alfa * Normalized(c1.calculateObservationProbability(), minObservationProbability, maxObservationProbability))
                		+ ((1 - alfa) * Normalized(c1.getPopularity(), 0, numPoints));
				double score2 = (alfa * Normalized(c2.calculateObservationProbability(), minObservationProbability, maxObservationProbability)) 
						+ ((1 - alfa) * Normalized(c2.getPopularity(), 0, numPoints));
//				System.out.println("Comparing: " + Double.valueOf(score1) + " to: " + Double.valueOf(score2));
//				System.out.println("Popularity c1: " + c1.getPopularity() + " Popularity c2: " + c2.getPopularity());
//				System.out.println("Popularity c1 normalized: " + Normalized(c1.getPopularity(), 0, numPoints) + " Popularity c2 normalized: " + Normalized(c2.getPopularity(), 0, numPoints));
//				System.out.println("Distance c1: " + c1.getDistance() + " Distance c2: " + c2.getDistance());
//				System.out.println("Observation Probability c1: " + c1.calculateObservationProbability() + " Observation Probability c2: " + c2.calculateObservationProbability());
//				System.out.println("Observation Probability c1 normalized: " + Normalized(c1.calculateObservationProbability(), minObservationProbability, maxObservationProbability) + " Observation Probability c2 normalized: " + Normalized(c2.calculateObservationProbability(), minObservationProbability, maxObservationProbability));
//				System.out.println("Compare result: " + Double.valueOf(score1).compareTo(Double.valueOf(score2)));
				return Double.valueOf(score1).compareTo(Double.valueOf(score2));
			}
    	};
    	
    	for(List<CandidatePoint> candidatePoints : aux){
//    		for(CandidatePoint candidatePoint: candidatePoints){
//    			System.out.println("Candidate: " + candidatePoint.toString());
//    		}
    		Collections.sort(candidatePoints, cmp);
    		CandidatePoint c1 = candidatePoints.get(candidatePoints.size() - 1);
//    		CandidatePoint c2 = candidatePoints.get(candidatePoints.size() - 2);
//    		double difference = c2.getDistance() - c1.getDistance();
    		CandidatePoint maxCandidatePoint = c1;
    		
//    		CandidatePoint maxCandidatePoint = Collections.max(candidatePoints, cmp);
//    		System.out.println("Collections.max result: " + (maxCandidatePoint.toString()));
//    		System.out.println("Test result: " + (maxCandidatePoint.getTrajectoryPoint().toString()));
    		result.addTrajectoryPoint(new TrajectoryPoint(maxCandidatePoint.getTrajectoryPoint().getID(), maxCandidatePoint.getX(), maxCandidatePoint.getY(), maxCandidatePoint.getTrajectoryPoint().getTime(), maxCandidatePoint.getEdgeID()));//(Collections.max(candidatePoints, cmp).
    	}
    	return result;
    }
    
    //Return the most Popular candidate given the maxDifference condition.
    public CandidatePoint checkPopularity(List<CandidatePoint> candidatePoints){
    	List<CandidatePoint> aux = new ArrayList<CandidatePoint>();
    	Collections.reverse(candidatePoints);
    	CandidatePoint closestCandidate = candidatePoints.get(0);
    	double difference = 0;
    	Comparator<CandidatePoint> cmp = new Comparator<CandidatePoint>(){
			@Override
			public int compare(CandidatePoint c1, CandidatePoint c2) {
				double score1 = Normalized(c1.getPopularity(), 0, numPoints);
				double score2 = Normalized(c2.getPopularity(), 0, numPoints);
				return Double.valueOf(score1).compareTo(Double.valueOf(score2));
			}
    	};
    	for(CandidatePoint candidatePoint : candidatePoints){
    		difference = Math.abs(closestCandidate.getDistance() - candidatePoint.getDistance());
    		if(difference <= maxDifference){
    			aux.add(candidatePoint);
    		}
    	}    	
		return Collections.max(aux, cmp);    	
    }
    
    public Trajectory MatchTest(double alfa) throws Exception{
    	Trajectory result = new Trajectory();
    	List<List<CandidatePoint>> aux = new ArrayList<List<CandidatePoint>>();
    	aux = prepareData();
    	Comparator<CandidatePoint> cmp = new Comparator<CandidatePoint>(){
			@Override
			public int compare(CandidatePoint c1, CandidatePoint c2) {
//				System.out.println("Comparing: " + c1.toString() + " to: " + c2.toString());
				double score1 = (alfa * Normalized(c1.calculateObservationProbability(), minObservationProbability, maxObservationProbability))
                		+ ((1 - alfa) * Normalized(c1.getPopularity(), 0, numPoints));
				double score2 = (alfa * Normalized(c2.calculateObservationProbability(), minObservationProbability, maxObservationProbability)) 
						+ ((1 - alfa) * Normalized(c2.getPopularity(), 0, numPoints));
//				System.out.println("Comparing: " + Double.valueOf(score1) + " to: " + Double.valueOf(score2));
//				System.out.println("Popularity c1: " + c1.getPopularity() + " Popularity c2: " + c2.getPopularity());
//				System.out.println("Popularity c1 normalized: " + Normalized(c1.getPopularity(), 0, numPoints) + " Popularity c2 normalized: " + Normalized(c2.getPopularity(), 0, numPoints));
//				System.out.println("Distance c1: " + c1.getDistance() + " Distance c2: " + c2.getDistance());
//				System.out.println("Observation Probability c1: " + c1.calculateObservationProbability() + " Observation Probability c2: " + c2.calculateObservationProbability());
//				System.out.println("Observation Probability c1 normalized: " + Normalized(c1.calculateObservationProbability(), minObservationProbability, maxObservationProbability) + " Observation Probability c2 normalized: " + Normalized(c2.calculateObservationProbability(), minObservationProbability, maxObservationProbability));
//				System.out.println("Compare result: " + Double.valueOf(score1).compareTo(Double.valueOf(score2)));
				return Double.valueOf(score1).compareTo(Double.valueOf(score2));
			}
    	};
    	
    	for(List<CandidatePoint> candidatePoints : aux){
//    		for(CandidatePoint candidatePoint: candidatePoints){
//    			System.out.println("Candidate: " + candidatePoint.toString());
//    		}
    		Collections.sort(candidatePoints, cmp);
//    		int i = 0;
//    		for(CandidatePoint candidatePoint: candidatePoints){    			
//    			System.out.println("Candidate" + ++i + ": " + candidatePoint.getDistance());
//    		}
    		
    		CandidatePoint maxCandidatePoint = checkPopularity(candidatePoints);
    		
//    		double difference = 0;
//    		CandidatePoint maxCandidatePoint;
//    		if(candidatePoints.isEmpty()){
//    			throw new Exception("Can't find any candidate");
//    		} else if(candidatePoints.size() >= 2){		
//    			CandidatePoint c1 = candidatePoints.get(candidatePoints.size() - 1);        		
//        		CandidatePoint c2 = candidatePoints.get(candidatePoints.size() - 2);
//        		difference = c2.getDistance() - c1.getDistance();
////        		System.out.println("Difference: " + difference);
//        		maxCandidatePoint = c1;
//        		if (difference < maxDifference){//difference == 0
//        			double score1 = //Normalized(c1.calculateObservationProbability(), minObservationProbability, maxObservationProbability) +
//    								Normalized(c1.getPopularity(), 0, numPoints);
//        			double score2 = //Normalized(c2.calculateObservationProbability(), minObservationProbability, maxObservationProbability) +
//    								Normalized(c2.getPopularity(), 0, numPoints);
//        			if (score2 > score1){
//        				maxCandidatePoint = c2;
//        			}
//        		}
//    		} else {
//    			maxCandidatePoint = candidatePoints.get(candidatePoints.size() - 1);
//    		}
    		
//    		System.out.println("First Candidate Distance: " + candidatePoints.get(candidatePoints.size() - 1).getDistance() +
//    				" Second Candidate Distance: " + candidatePoints.get(candidatePoints.size() - 2).getDistance());
//    		System.out.println("Difference: " + difference);
    		
//    		CandidatePoint maxCandidatePoint = Collections.max(candidatePoints, cmp);
//    		System.out.println("Collections.max result: " + (maxCandidatePoint.toString()));
//    		System.out.println("Test result: " + (maxCandidatePoint.getTrajectoryPoint().toString()));
    		result.addTrajectoryPoint(new TrajectoryPoint(maxCandidatePoint.getTrajectoryPoint().getID(), maxCandidatePoint.getX(), maxCandidatePoint.getY(), maxCandidatePoint.getTrajectoryPoint().getTime(), maxCandidatePoint.getEdgeID()));//(Collections.max(candidatePoints, cmp).
    	}
    	return result;
    }
}
