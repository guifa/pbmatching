package pbmatching;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.data.postgis.PostgisNGDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.FactoryRegistry;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Fill;
import org.geotools.styling.Graphic;
import org.geotools.styling.LineSymbolizer;
import org.geotools.styling.Mark;
import org.geotools.styling.PointSymbolizer;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.Rule;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Stroke;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.swing.action.SafeAction;
import org.geotools.swing.data.JDataStoreWizard;
import org.geotools.swing.styling.JSimpleStyleDialog;
import org.geotools.swing.wizard.JWizard;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import gui.JCustomMapFrame;

public class GUITest {

 static StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
 static FilterFactory filterFactory = CommonFactoryFinder.getFilterFactory();
 private DataStore pgDatastore;
 private JCustomMapFrame mapFrame;
 private JComboBox tableCBox;
 private JMenuBar menubar;
 private MapContent map;

 public static void main(String[] args) throws Exception {
     GUITest me = new GUITest();
     me.displayMap();
 }

 // docs end main

 // docs start display
 /**
  * Prompts the user for a shapefile (unless a filename is provided
  * on the command line; then creates a simple Style and displays
  * the shapefile on screen
  */
 private void displayMap() throws Exception {
	      
	 // Set cross-platform look & feel for compatability
     UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
     map = new MapContent();
     map.setTitle("PBMatching");
     mapFrame = new JCustomMapFrame(map); 
     mapFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     mapFrame.getContentPane().setLayout(new BorderLayout());
     
     menubar = new JMenuBar();
     mapFrame.setJMenuBar(menubar);

     JMenu fileMenu = new JMenu("File");
     menubar.add(fileMenu);
     
     JMenu addNewLayerMenu = new JMenu("Add New Layer");
     menubar.add(addNewLayerMenu);
     
     JMenu runMenu = new JMenu("Run");
     menubar.add(runMenu);

     tableCBox = new JComboBox();
     menubar.add(tableCBox);
     
     mapFrame.pack();
     
     //File Menu
     fileMenu.add(new SafeAction("Connect to PostGIS database...") {
         public void action(ActionEvent e) throws Throwable {
             connect();
         }
     });
     
     //Add New Layer Menu     
     addNewLayerMenu.add(new SafeAction("Add New Map Layer") {
         public void action(ActionEvent e) throws Throwable {
        	 addNewMapLayer();
         }
     });
     
     addNewLayerMenu.add(new SafeAction("Add New Trajectory Layer") {
         public void action(ActionEvent e) throws Throwable {
        	 addNewTrajectoryLayer();
         }
     });
     
     //Run Menu
     runMenu.add(new SafeAction("PBMatching") {		
		@Override
		public void action(ActionEvent e) throws Throwable {
			pbMatching();
		}
	});
     
     
     

     
     
//     SimpleFeatureSource edges = pgDatastore.getFeatureSource(tableName); 
//     Style styleEdges = createLineStyle();
//     Layer layerEdges = new FeatureLayer(edges, styleEdges);
//     SimpleFeatureSource nodes = pgDatastore.getFeatureSource("fortaleza_nodes"); 
//     Style styleNodes = createPointStyle();
//     Layer layerNodes = new FeatureLayer(nodes, styleNodes);
//	   
//     
//		
     
//     map.addLayer(layerEdges); 
//     map.addLayer(layerNodes); 
//     
     mapFrame.enableToolBar(true); 
     mapFrame.enableStatusBar(true);
     mapFrame.enableLayerTable(true);
     
     JToolBar toolBar = mapFrame.getToolBar();
//     
//     // Now display the map
     mapFrame.setSize(800, 600);
     mapFrame.setVisible(true);
 }



 // docs start alternative
 /**
  * Here is a programmatic alternative to using JSimpleStyleDialog to
  * get a Style. This methods works out what sort of feature geometry
  * we have in the shapefile and then delegates to an appropriate style
  * creating method.
  */
 private void connect() throws Exception {
	//create the PostGISDataStore 
     Map<String, Object> params = new HashMap<String, Object> (); 
     params.put("dbtype", "postgis"); 
     params.put("host", "localhost"); 
     params.put("port", 5432);
     params.put( "schema", "public");
     params.put("database", "postgres");
     params.put("user", "postgres");
     params.put("passwd", "098765");
     pgDatastore = DataStoreFinder.getDataStore(params);
     if (pgDatastore == null) {
         JOptionPane.showMessageDialog(null, "Could not connect - check parameters");
     }
     updateUI();
     
 }

 
 private void updateUI() throws Exception {
     ComboBoxModel cbm = new DefaultComboBoxModel(pgDatastore.getTypeNames());
     tableCBox.setModel(cbm);
 }
 
 private void addNewMapLayer() throws IOException{
	 String tableName = (String) tableCBox.getSelectedItem();
	 SimpleFeatureSource newTable = pgDatastore.getFeatureSource(tableName); 
	 Layer newMapLayer = new FeatureLayer(newTable, createLineStyle());
	 map.addLayer(newMapLayer);
	 
 }
 
 private void addNewTrajectoryLayer() throws IOException{
	 String tableName = (String) tableCBox.getSelectedItem();
	 SimpleFeatureSource newTable = pgDatastore.getFeatureSource(tableName); 
	 Layer newTrajectoryLayer = new FeatureLayer(newTable, createPointStyle());
	 map.addLayer(newTrajectoryLayer);
	 
 }
 
 private void pbMatching(){
	 
 }
 
 private Style createStyle(FeatureSource featureSource) {
     SimpleFeatureType schema = (SimpleFeatureType)featureSource.getSchema();
     Class geomType = schema.getGeometryDescriptor().getType().getBinding();

     if (Point.class.isAssignableFrom(geomType)){
         return createPointStyle();

     } else if (LineString.class.isAssignableFrom(geomType)
             || MultiLineString.class.isAssignableFrom(geomType)) {
         return createLineStyle();

     } else {
         return createPolygonStyle();
     }
 }
 private Style createStyle2(FeatureSource featureSource) {
     SimpleFeatureType schema = (SimpleFeatureType)featureSource.getSchema();
     Class geomType = schema.getGeometryDescriptor().getType().getBinding();

     if (Polygon.class.isAssignableFrom(geomType)
             || MultiPolygon.class.isAssignableFrom(geomType)) {
         return createPolygonStyle();

     } else if (LineString.class.isAssignableFrom(geomType)
             || MultiLineString.class.isAssignableFrom(geomType)) {
         return createLineStyle();

     } else {
         return createPointStyle();
     }
 }

 /**
  * Create a Style to draw polygon features with a thin blue outline and
  * a cyan fill
  */
 private Style createPolygonStyle() {

     // create a partially opaque outline stroke
     Stroke stroke = styleFactory.createStroke(
             filterFactory.literal(Color.BLUE),
             filterFactory.literal(1),
             filterFactory.literal(0.5));

     // create a partial opaque fill
     Fill fill = styleFactory.createFill(
             filterFactory.literal(Color.CYAN),
             filterFactory.literal(0.5));

     /*
      * Setting the geometryPropertyName arg to null signals that we want to
      * draw the default geomettry of features
      */
     PolygonSymbolizer sym = styleFactory.createPolygonSymbolizer(stroke, fill, null);

     Rule rule = styleFactory.createRule();
     rule.symbolizers().add(sym);
     FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
     Style style = styleFactory.createStyle();
     style.featureTypeStyles().add(fts);

     return style;
 }
 
 /**
  * Create a Style to draw line features as thin blue lines
  */
 private Style createLineStyle() {
     Stroke stroke = styleFactory.createStroke(
             filterFactory.literal(Color.BLUE),
             filterFactory.literal(1));

     /*
      * Setting the geometryPropertyName arg to null signals that we want to
      * draw the default geomettry of features
      */
     LineSymbolizer sym = styleFactory.createLineSymbolizer(stroke, null);

     Rule rule = styleFactory.createRule();
     rule.symbolizers().add(sym);
     FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
     Style style = styleFactory.createStyle();
     style.featureTypeStyles().add(fts);

     return style;
 }

 /**
  * Create a Style to draw point features as circles with blue outlines
  * and cyan fill
  */
 private Style createPointStyle() {
     Graphic gr = styleFactory.createDefaultGraphic();

     Mark mark = styleFactory.getCircleMark();

     mark.setStroke(styleFactory.createStroke(
             filterFactory.literal(Color.BLUE), filterFactory.literal(1)));

     mark.setFill(styleFactory.createFill(filterFactory.literal(Color.CYAN)));

     gr.graphicalSymbols().clear();
     gr.graphicalSymbols().add(mark);
     gr.setSize(filterFactory.literal(5));

     /*
      * Setting the geometryPropertyName arg to null signals that we want to
      * draw the default geomettry of features
      */
     PointSymbolizer sym = styleFactory.createPointSymbolizer(gr, null);

     Rule rule = styleFactory.createRule();
     rule.symbolizers().add(sym);
     FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
     Style style = styleFactory.createStyle();
     style.featureTypeStyles().add(fts);

     return style;
 }

}