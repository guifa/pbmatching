package geometry;

public class Point{
    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    public String toPostgis(){
    	return "'ST_SetSRID(ST_MakePoint(" + x + "," + y + "),4326))'";
    }

    @Override
    public String toString() {
        return "'POINT(" + x + " " + y + ")'";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (Double.compare(point.x, x) != 0) return false;
        if (Double.compare(point.y, y) != 0) return false;

        return true;
    }   
}