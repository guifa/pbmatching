package geometry;

public class CandidatePoint extends Point{
	
	//ID do Ponto Candidato.
	private int ID;
	//Distancia do Ponto Candidato at� o Ponto Original da Trajet�ria.
	private double distance;
	//Aresta que o Ponto Candidato est� localizada.
	private long edgeID;
	//Ponto da Trajet�ria a partir do qual o Ponto Candidato foi criado.
	private TrajectoryPoint trajectoryPoint;
	//Popularidade da rua onde se encontra o Ponto Candidato.
	private double popularity;
	
	public CandidatePoint(int id, double x, double y, TrajectoryPoint trajectoryPoint, long edgeID, double distance, double popularity) {
		super(x, y);
		this.ID = id;
		this.trajectoryPoint = trajectoryPoint;
		this.edgeID = edgeID;
		this.distance = distance;
		this.popularity = popularity;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public TrajectoryPoint getTrajectoryPoint() {
		return this.trajectoryPoint;
	}
	
	public long getEdgeID() {
		return this.edgeID;
	}
	
	public double getDistance() {
		return this.distance;
	}
	
	public double getPopularity() {
		return this.popularity;
	}
	
	public double Normalized(double e, double Emin, double Emax){
		return (e - Emin)/(Emax - Emin);    	
    }
	
	public double calculateObservationProbability(){
        double sigma = 20;       
        return Math.exp(-distance * distance / (2 * sigma * sigma)) / (sigma * Math.sqrt(Math.PI * 2));        
    }
	
	public double calculateObservationProbability2(){
        double sigma = 20;       
        return Math.exp(-distance * distance / (2 * sigma * sigma)) / (sigma * Math.sqrt(Math.PI * 2));        
    }
	
	public double calculateScore(int numPoints, double minObservationProbability, double maxObservationProbability){
		if (this.distance <= 5){
			return Normalized(this.calculateObservationProbability(), minObservationProbability, maxObservationProbability);
		}else{
			return Normalized(this.calculateObservationProbability(), minObservationProbability, maxObservationProbability) + 
					Normalized(this.popularity, 0, numPoints);
		}
		
	}
}
