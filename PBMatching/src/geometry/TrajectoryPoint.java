package geometry;

public class TrajectoryPoint extends Point {
	//TrajectoryPoint ID.
	private int ID;
	//Time stamp of Trajectory ID.
	private double time;
	//-1 when its a TrajectoryPoint that is not in a ground truth trajectory or matched trajectory, 
	//otherwise its the value of edgeID where the TrajectoryPoint is on.
	private long edgeID;

	public TrajectoryPoint(int id, double x, double y, double time, long edgeID) {
		super(x, y);
		this.ID = id;
		this.time = time;
		this.edgeID = edgeID;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public double getTime() {
		return time;
	}
	
	public long getEdgeID() {
		return edgeID;
	}

}
