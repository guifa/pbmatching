package geometry;

public class Edge {
	
	private final Point start;
	private final Point end;
	private final double cost;
	
	public Edge(Point start, Point end, double cost){
		this.start = start;
		this.end = end;
		this.cost = cost;
	}
	
	public Point getStart(){
		return this.start;
	}
	
	public Point getEnd(){
		return this.end;
	}
	
	public double getCost(){
		return this.cost;
	}
	
	public String toPostgis(){
		return "'ST_MakeLine(ST_MakePoint(" + start.getX() + ", " + start.getY() + "), ST_MakePoint(" + end.getX() + ", " + end.getY() + "))'";
	}
	
	@Override
	public String toString(){
		return "'LINESTRING(" + start.getX() + " " + start.getY() + "," + end.getX() + " " + end.getY() + ")'";
	}

	
}
