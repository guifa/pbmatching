package geometry;

import java.util.List;

public class RoadNetwork {
	
	private List<Edge> edges;
	private List<Point> vertices;
	
	public RoadNetwork(List<Edge> edges, List<Point> vertices){
		this.edges = edges;
		this.vertices = vertices;
	}
	
	public List<Edge> getEdges() {
		return edges;
	}
	
	public List<Point> getVertices() {
		return vertices;
	}	

}
