package geometry;

import java.util.ArrayList;
import java.util.List;

public class Trajectory {	
	private List<TrajectoryPoint> trajectoryPoints;
	
	public Trajectory(){
		this.trajectoryPoints = new ArrayList<>();
	}
	
	public Trajectory(List<TrajectoryPoint> trajectoryPoints){
		this.trajectoryPoints = trajectoryPoints;
	}
	
	public void addTrajectoryPoint(TrajectoryPoint trajectoryPoint){
		this.trajectoryPoints.add(trajectoryPoint);
	}
	
	public Trajectory trajectoryFromCandidatePoints(List<CandidatePoint> candidatePoints){
		List<TrajectoryPoint> trajectoryPoints = new ArrayList<>();
		for(CandidatePoint candidatePoint: candidatePoints){
			trajectoryPoints.add(candidatePoint.getTrajectoryPoint());
		}
		Trajectory trajectory = new Trajectory(trajectoryPoints);
		return trajectory;		
	}
	
	public List<TrajectoryPoint> getTrajectoryPoints() {
		return this.trajectoryPoints;
	}
	
	public void printTrajectory(){
		for(TrajectoryPoint trajectoryPoint : trajectoryPoints){
			System.out.println(trajectoryPoint.toString());
		}
	}
	
}
